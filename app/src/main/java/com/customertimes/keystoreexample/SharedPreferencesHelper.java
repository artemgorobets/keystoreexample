package com.customertimes.keystoreexample;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Artem Gorobets
 * 9/1/14.
 */
public class SharedPreferencesHelper {

    private static final String PREF_FILE = "Userdata";
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;
    private String password = null;
    private String login = null;
    private String marker = null;
    private String token = null;

    private static Map<Context, SharedPreferencesHelper> instances = new HashMap<Context, SharedPreferencesHelper>();

    public SharedPreferencesHelper(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static SharedPreferencesHelper getInstance(Context context) {
        if (!instances.containsKey(context))
            instances.put(context, new SharedPreferencesHelper(context));
        return instances.get(context);
    }

    public String getPassword() {
        return sharedPreferences.getString("password", null);
    }

    public void setPassword(String password) {
        this.password = password;
        editor.putString("password", password);
        editor.commit();
    }

    public String getLogin() {
        return sharedPreferences.getString("login", null);
    }

    public void setLogin(String login) {
        this.login = login;
        editor.putString("login", login);
        editor.commit();
    }

    public String getMarker() {
        return sharedPreferences.getString("marker", null);
    }

    public void setMarker(String marker) {
        this.marker = marker;
        editor.putString("marker", marker);
        editor.commit();
    }

    public String getToken() {
        return sharedPreferences.getString("token", null);
    }

    public void setToken(String token) {
        this.token = token;
        editor.putString("token", token);
        editor.commit();
    }
}
