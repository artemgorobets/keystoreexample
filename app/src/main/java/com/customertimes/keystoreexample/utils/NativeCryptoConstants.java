package com.customertimes.keystoreexample.utils;

/**
 * Created by Artem Gorobets
 * 8/28/14.
 */
public class NativeCryptoConstants {

    public static final int EVP_PKEY_RSA = 6; // NID_rsaEcnryption
    public static final int EVP_PKEY_DSA = 116; // NID_dsa
    public static final int EVP_PKEY_DH = 28; // NID_dhKeyAgreement
    public static final int EVP_PKEY_EC = 408; // NID_X9_62_id_ecPublicKey
    public static final int EVP_PKEY_HMAC = 855; // NID_hmac
    public static final int EVP_PKEY_CMAC = 894; // NID_cmac
    public static final int outputKeyLength = 256;


    private NativeCryptoConstants() {
    }
}