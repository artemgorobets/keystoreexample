package com.customertimes.keystoreexample;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.customertimes.keystoreexample.keystore.KeyStore;
import com.customertimes.keystoreexample.keystore.KeyStoreJb43;
import com.customertimes.keystoreexample.keystore.KeyStoreKk;
import com.customertimes.keystoreexample.utils.Crypto;
import com.customertimes.keystoreexample.utils.NativeCryptoConstants;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class LoginActivity extends Activity {

    private static final boolean IS_JB43 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    private static final boolean IS_JB = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    private static final boolean IS_KK = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
    public static final String OLD_UNLOCK_ACTION = "android.credentials.UNLOCK";
    public static final String UNLOCK_ACTION = "com.android.credentials.UNLOCK";
    private static final String TAG = "CRYPTO";

    String keyName = "SecretKey";
    Button saveDataBtn;
    Button getDataBtn;
    Button clearButton;
    EditText editLogin;
    EditText editPassword;
    EditText editToken;
    EditText editMarker;
    TextView labelEncryptedLogin;
    TextView labelEncryptedPassword;
    TextView labelEncryptedMarker;
    TextView labelEncryptedToken;
    KeyStore keyStore;
    SecretKey secretKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViews();
        setupKeyStore();
        setListeners();
        try {
            generateKey();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        getStorageInfo();
        unlockKeyStore()
;    }

    private void findViews(){
        saveDataBtn = (Button) findViewById(R.id.button_save_data);
        getDataBtn = (Button) findViewById(R.id.button_get_data);
        clearButton = (Button) findViewById(R.id.button_clear);
        editLogin = (EditText) findViewById(R.id.edit_login);
        editPassword = (EditText) findViewById(R.id.edit_password);
        editMarker = (EditText) findViewById(R.id.edit_marker);
        editToken = (EditText) findViewById(R.id.edit_token);
        labelEncryptedLogin = (TextView) findViewById(R.id.label_ecrypted_login);
        labelEncryptedPassword = (TextView) findViewById(R.id.label_ecrypted_password);
        labelEncryptedMarker = (TextView) findViewById(R.id.label_ecrypted_marker);
        labelEncryptedToken = (TextView) findViewById(R.id.label_ecrypted_token);
    }

    private void setListeners(){
        saveDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storeUserData(
                        editLogin.getText().toString(),
                        editPassword.getText().toString(),
                        editMarker.getText().toString(),
                        editToken.getText().toString());
            }
        });

        getDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUserData();
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFields();
            }
        });
    }

    private void clearFields() {
        editLogin.setText("");
        editPassword.setText("");
        editMarker.setText("");
        editToken.setText("");
        labelEncryptedLogin.setText("");
        labelEncryptedPassword.setText("");
        labelEncryptedMarker.setText("");
        labelEncryptedToken.setText("");
    }

    private void setupKeyStore() {
        keyStore = KeyStore.getInstance();
        if (IS_KK) {
            keyStore = KeyStoreKk.getInstance();
        } else if (IS_JB43) {
            keyStore = KeyStoreJb43.getInstance();
        } else {
            keyStore = KeyStore.getInstance();
        }

        if (keyStore.state() == KeyStore.State.LOCKED) {
            Log.d(TAG, "Try to unlock keystore");
            unlockKeyStore();
        }
    }

    private void storeUserData(String login, String password, String marker, String token){
        ArrayList<String> encryptedUserData;
        encryptedUserData = encryptUserData(login, password, marker, token);

        login = encryptedUserData.get(0);
        password = encryptedUserData.get(1);
        marker = encryptedUserData.get(2);
        token = encryptedUserData.get(3);

        SharedPreferencesHelper.getInstance(this).setLogin(login);
        SharedPreferencesHelper.getInstance(this).setPassword(password);
        SharedPreferencesHelper.getInstance(this).setMarker(marker);
        SharedPreferencesHelper.getInstance(this).setToken(token);
    }

    private void getUserData(){
        byte[] keyBytes = keyStore.get(keyName);
        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
        editLogin.setText(Crypto.decryptAesCbc(SharedPreferencesHelper.getInstance(this).getLogin(), key));
        editPassword.setText(Crypto.decryptAesCbc(SharedPreferencesHelper.getInstance(this).getPassword(), key));
        editMarker.setText(Crypto.decryptAesCbc(SharedPreferencesHelper.getInstance(this).getMarker(), key));
        editToken.setText(Crypto.decryptAesCbc(SharedPreferencesHelper.getInstance(this).getToken(), key));

        displayEncryptedData();
    }

    private ArrayList<String> encryptUserData(String login, String password, String marker, String token){
        ArrayList<String> encryptedUserData = new ArrayList<String>();
        SecretKey key = Crypto.generateAesKey();
        boolean success = keyStore.put(keyName, key.getEncoded());
        Log.d(TAG, "put key success: " + success);
        encryptedUserData.add(Crypto.encryptAesCbc(login, key));
        encryptedUserData.add(Crypto.encryptAesCbc(password, key));
        encryptedUserData.add(Crypto.encryptAesCbc(marker, key));
        encryptedUserData.add(Crypto.encryptAesCbc(token, key));

        return encryptedUserData;
    }

    private void unlockKeyStore() {
        if (keyStore.state() == KeyStore.State.UNLOCKED) {
            return;
        }
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                startActivity(new Intent(OLD_UNLOCK_ACTION));
            } else {
                startActivity(new Intent(UNLOCK_ACTION));
            }
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "No UNLOCK activity: " + e.getMessage(), e);
            Toast.makeText(this, "No keystore unlock activity found.",
                    Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private void generateKey() throws NoSuchAlgorithmException {

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        SecureRandom secureRandom = new SecureRandom();
        keyGenerator.init(NativeCryptoConstants.outputKeyLength, secureRandom);

        secretKey = keyGenerator.generateKey();
        Log.d(TAG, "key was generated");
        Log.d(TAG, secretKey.toString());

    }

    private void getStorageInfo(){
        Log.d(TAG, keyStore.state().toString());
        Log.d(TAG, keyStore.saw("").toString());
        displayKeys();
    }

    private void displayKeys() {
        if (!keyStore.isEmpty()) {
            String[] keyNames = keyStore.saw("");
            Log.d(TAG, "Keys in store: " + String.valueOf(keyNames.length));
            for (int i = 0; i < keyNames.length; i++) {
                Log.d(TAG, "Key #" + String.valueOf(i) + ": " + keyNames[i].toString());
            }
        } else {
            Log.d(TAG, "Keystore is empty");
        }
    }

    private void displayEncryptedData(){
        labelEncryptedLogin.setText("Login:\n" + SharedPreferencesHelper.getInstance(this).getLogin());
        labelEncryptedPassword.setText("Password:\n" + SharedPreferencesHelper.getInstance(this).getPassword());
        labelEncryptedMarker.setText("Marker:\n" + SharedPreferencesHelper.getInstance(this).getMarker());
        labelEncryptedToken.setText("Token:\n" + SharedPreferencesHelper.getInstance(this).getToken());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
